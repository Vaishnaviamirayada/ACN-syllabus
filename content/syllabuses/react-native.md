---
title: React Native
_db_id: 82
---

- {{% contentlink path="react-native/introduction" %}}
- {{% contentlink path="react-native/getting-set-up" %}}
- {{% contentlink path="react-native/expo-tutorial-project" flavour="javascript" %}}
- {{% contentlink path="react-native/simple-vario/intro" flavour="javascript" %}}
- {{% contentlink path="react-native/simple-vario/data-display" flavour="javascript" %}}



------

# Placeholder content

Everything below this line will be turned into proper syllabus content a bit later. For now this is just a dumping ground

## Component libraries

There are a few to choose from: https://docs.expo.dev/guides/userinterface/
Use NativeBase because the docs are great and it's easy to set up




## native functionality project

Voice recorder?
Flight log (start flight end flight)

location + firebase.
Eg there is a power outage here now
I have power here now


## firebase

https://docs.expo.dev/guides/using-firebase/
https://docs.expo.dev/introduction/walkthrough





## Courses:

https://learning.edx.org/course/course-v1:HarvardX+CS50M+Mobile/home
https://web.stanford.edu/class/cs47si/#overview
https://web.stanford.edu/class/cs47/
